package com.basic.base

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import my.forest.ForestApp

open class BasePresenter<ViewType : MvpView> : MvpPresenter<ViewType>() {

    fun getString(resId:Int) :String {
        return ForestApp.applicationContext().getString(resId)
    }
}