package com.basic.base

import android.app.Activity
import android.util.Log
import com.android.billingclient.api.*
import com.basic.utils.LogUtils
import my.forest.ForestApp
import com.android.billingclient.api.BillingFlowParams
import io.reactivex.subjects.PublishSubject

object BillingManager {

    private val TAG: String = "BillingManager"
    val BILLING_MANAGER_NOT_INITIALIZED = -1

    private lateinit var billingClient: BillingClient
    private var isServiceConnected: Boolean = false

    private var billingClientResponseCode = BILLING_MANAGER_NOT_INITIALIZED
    private val pendingActions: MutableList<() -> Unit> = mutableListOf()
    private val onPurchaseUpdated:PublishSubject<String> = PublishSubject.create()

    init {
        initBillingClient()
    }

    fun onPurchaseUpdateObservable():PublishSubject<String> {
        return onPurchaseUpdated
    }

    fun initBillingClient() {

        billingClient = BillingClient.newBuilder(ForestApp.applicationContext())
                .setListener(object : PurchasesUpdatedListener {

                    override fun onPurchasesUpdated(responseCode: Int, purchases: MutableList<Purchase>?) {
                        LogUtils.log(TAG, "onPurchasesUpdated : responseCode : " + responseCode.toString() + " ..... " + purchases.toString())
                        if (responseCode == BillingClient.BillingResponse.OK) {
                            if (purchases != null && purchases.size > 0) {
                                onPurchaseUpdated.onNext(purchases.get(0).sku ?: "")
                            }
                        }
                    }
                })
                .build();

        fun action() {}
        startServiceConnection({action()})
    }

    fun getProductsDetails(skusList:List<String>, listener: SkuDetailsResponseListener) {
        querySkuDetailsAsync(BillingClient.SkuType.INAPP,
                skusList,
                listener)
    }

    private fun querySkuDetailsAsync(@BillingClient.SkuType itemType: String,
                             skuList: List<String>,
                             listener: SkuDetailsResponseListener) {

        fun queryRequest() {
            val params = SkuDetailsParams.newBuilder()
                    .setType(itemType)
                    .setSkusList(skuList)

            billingClient.querySkuDetailsAsync(params.build(), listener)
        }

        executeServiceRequest({queryRequest()})
    }

    private fun consume(sku: String, listener: (Int) -> Unit) {
        LogUtils.log(TAG,"try consume")
        getPurchase { purchaseResult ->
            val purchasesList = purchaseResult.purchasesList
            if (purchasesList != null && purchasesList.size > 0) {

                LogUtils.log(TAG,"purchasesList != null")
                val purchase = purchasesList.find { it.sku.equals(sku) }
                if (purchase != null) {
                    val token = purchase.purchaseToken

                    LogUtils.log(token)
                    billingClient.consumeAsync(token) { responseCode, purchaseToken ->
                        listener.invoke(responseCode)
                    }
                } else {
                    listener.invoke(-3)
                }
            } else {
                listener.invoke(-4)
            }
        }
    }

    fun purchase(sku:String, activity: Activity, responseListener:(responseCode:Int) -> Unit) {

        fun runBillingFlow() {
            val params = BillingFlowParams.newBuilder()
                    .setSku(sku)
                    .setType(BillingClient.SkuType.INAPP)
                    .build()

            @BillingClient.BillingResponse val response:Int = billingClient.launchBillingFlow(activity, params)
            LogUtils.log(TAG, "launchBillingFlow : response : " + response.toString())
            responseListener.invoke(response)
        }

        fun queryRequest() {
            val needConsume = false
            if (needConsume) {
                consume(sku, {code ->

                    if (code == BillingClient.BillingResponse.OK || code == -3 || code == -4) {
                        LogUtils.log(TAG,"consumed")
                        runBillingFlow()
                    } else {
                        responseListener.invoke(code)
                    }
                })

            } else {
                runBillingFlow()
            }
        }

        executeServiceRequest({queryRequest()})
    }

    fun getPurchasesAsync(listener: PurchaseHistoryResponseListener) {
        LogUtils.log(TAG, "getPurchasesAsync")
        fun queryRequest() {
            LogUtils.log(TAG, "getPurchasesAsync queryRequest")
            billingClient.queryPurchaseHistoryAsync(BillingClient.SkuType.INAPP, listener)
        }

        executeServiceRequest { queryRequest() }
    }

    fun getPurchase(purchaseResultFetcher: (Purchase.PurchasesResult) -> Unit)  {

        LogUtils.log(TAG, "getPurchase")

        fun queryRequest() {
            LogUtils.log(TAG, "getPurchase queryRequest")
            val result:Purchase.PurchasesResult = billingClient.queryPurchases(BillingClient.SkuType.INAPP)
            LogUtils.log(BillingManager.TAG, "getPurchase: result size : " + result.purchasesList.size.toString())

            purchaseResultFetcher.invoke(result)
        }

        executeServiceRequest { queryRequest() }
    }

    fun startServiceConnection(onServiceConnected: () -> Unit) {
        LogUtils.log(TAG, "startServiceConnection")
        billingClient.startConnection(object : BillingClientStateListener {

            override fun onBillingSetupFinished(@BillingClient.BillingResponse billingResponseCode: Int) {
                Log.d(TAG, "Setup finished. Response code: " + billingResponseCode)

                if (billingResponseCode == BillingClient.BillingResponse.OK) {
                    isServiceConnected = true
                    onServiceConnected()
                    onBillingSetupFinishedAction()

                } else if (billingResponseCode == BillingClient.BillingResponse.DEVELOPER_ERROR) {

                    pendingActions.add(onServiceConnected)
                }
                billingClientResponseCode = billingResponseCode
            }

            override fun onBillingServiceDisconnected() {
                isServiceConnected = false
                fun action(){}
                startServiceConnection({action()})
            }
        })
    }

    fun onBillingSetupFinishedAction() {
        execPendingActions()
    }

    private fun execPendingActions() {
        LogUtils.log(TAG, "execPendingActions")
        val iterator = pendingActions.listIterator()

        LogUtils.log(TAG, "pendingActions : size : " + pendingActions.size.toString())
        while (iterator.hasNext()) {
            iterator.next().invoke()
            iterator.remove()
        }
    }

    private fun executeServiceRequest(action: () -> Unit) {
        LogUtils.log(TAG, "executeServiceRequest")
        if (isServiceConnected) {
            LogUtils.log(TAG, "isServiceConnected")
            action()
        } else {
            startServiceConnection(action)
        }
    }
}