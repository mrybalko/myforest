package com.basic.base

import android.content.Context
import com.arellomobile.mvp.MvpAppCompatActivity
import io.github.inflationx.viewpump.ViewPumpContextWrapper

open class BaseActivity : MvpAppCompatActivity() {

    private val TAG: String = "BaseActivity"

    private val navigator:Navigator

    init {
        navigator = Navigator()
    }

    fun getNavigator(): Navigator {
        return navigator;
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase))
    }
}
