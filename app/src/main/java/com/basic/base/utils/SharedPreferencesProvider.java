package com.basic.base.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.basic.utils.Singleton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import my.forest.data.TreeData;

public class SharedPreferencesProvider {

    private final static String SHARED_PREFERENCES_KEY = "SPProvider";

    private final static String KEY_TREES = "key_trees";

    private static SharedPreferences sharedPreferences;
    private static Gson gson;

    private static Singleton<SharedPreferencesProvider> _instanceHolder = new Singleton<>(SharedPreferencesProvider::new);

    public static SharedPreferencesProvider instance() {
        return _instanceHolder.instance();
    }

    private SharedPreferencesProvider() {
    }

    public static void init(Context context){
        sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_KEY, Context.MODE_PRIVATE);
        gson = new Gson();
    }

    public List<TreeData> getTrees() {

        Type listType = new TypeToken<List<TreeData>>() {}.getType();
        String oldTreesString = sharedPreferences.getString(KEY_TREES, "[]");

        return gson.fromJson(oldTreesString, listType);
    }

    public void removeTree(TreeData data){
        List<TreeData> oldTrees = getTrees();
        oldTrees.remove(data);
        setTrees(oldTrees);
    }

    public void addTree(TreeData data){
        List<TreeData> oldTrees = getTrees();
        oldTrees.add(data);
        setTrees(oldTrees);
    }

    public void setTrees(List<TreeData> trees) {
        String treesString = gson.toJson(trees);
        sharedPreferences
                .edit()
                .putString(KEY_TREES, treesString)
                .apply();
    }
}

