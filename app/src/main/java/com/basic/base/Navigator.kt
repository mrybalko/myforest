package com.basic.base

import android.app.Activity
import android.content.Intent
import my.forest.ui.addingmenu.AddingMenuActivity
import my.forest.ui.choosers.premium.ChoosePremiumTreesActivity
import my.forest.ui.choosers.selfgrown.ChooseSelfGrownTreesActivity

class Navigator {

    private fun start(activity: Activity,
                      intent: Intent): Boolean {
        activity.startActivity(intent)
        return true
    }

    fun goToAddingMenu(activity: Activity) {
        start(activity, AddingMenuActivity.newInstance(activity))
    }

    fun goToSelfGrownTrees(activity: Activity) {
        start(activity, ChooseSelfGrownTreesActivity.newInstance(activity))
    }

    fun goToPremiumTrees(activity: Activity) {
        start(activity, ChoosePremiumTreesActivity.newInstance(activity))
    }
}