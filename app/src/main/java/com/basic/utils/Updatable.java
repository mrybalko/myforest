package com.basic.utils;

public interface Updatable {
    void update();
}