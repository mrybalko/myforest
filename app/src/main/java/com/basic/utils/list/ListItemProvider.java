package com.basic.utils.list;

import android.content.Context;
import android.view.View;

public interface ListItemProvider<DataType> {
    BaseViewHolder<DataType> item(Context context, View view);
}