package com.basic.utils.list;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import java.util.ArrayList;

public class ListAdapter<DataType> extends RecyclerView.Adapter<BaseViewHolder<DataType>> {

    private DataSet<DataType> dataSet;
    private ItemLayoutProvider layoutProvider;
    private ListItemProvider<DataType> itemProvider;
    private AdapterView.OnItemClickListener onItemClickListener;

    public static <DataType> ListAdapter<DataType> createEmptyAdapter(final ListItemProvider<DataType> itemProvider,
                                                                      final ItemLayoutProvider provider) {
        return new ListAdapter<>(itemProvider, provider, new ListDataSet<>(new ArrayList<>()));
    }

    public ListAdapter(final ListItemProvider<DataType> itemProvider,
                       final ItemLayoutProvider provider,
                       final DataSet<DataType> dataSet) {
        this.itemProvider = itemProvider;
        layoutProvider = provider;
        this.dataSet = dataSet;
    }

    public void updateData(DataSet<DataType> newDataSet) {
        dataSet = newDataSet;
        notifyDataSetChanged();
    }

    @Override
    public BaseViewHolder<DataType> onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(layoutProvider.layoutId(), parent, false);
        BaseViewHolder<DataType> holder = itemProvider.item(context, view);
        holder.setOnHolderClickListener(onItemClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder<DataType> holder, int position) {
        holder.onBind(dataSet.getItem(position));
    }

    public DataType getItem(final int position) {
        return dataSet.getItem(position);
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public void setOnItemClickListener(@NonNull final AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public void onViewRecycled(BaseViewHolder<DataType> holder) {
        super.onViewRecycled(holder);
        holder.clear();
    }

    public void updateItemProvider(final ItemLayoutProvider provider) {
        layoutProvider = provider;
    }
}
