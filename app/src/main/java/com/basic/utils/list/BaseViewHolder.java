package com.basic.utils.list;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;

public class BaseViewHolder<DataType> extends RecyclerView.ViewHolder {

    @Nullable
    private AdapterView.OnItemClickListener onClickListener;
    private View rootLayout;
    private DataType data;

    public BaseViewHolder(View view) {
        super(view);
        rootLayout = view;
        rootLayout.setOnClickListener(v -> {
            if (onClickListener != null) {
                onClickListener.onItemClick(null, rootLayout, getAdapterPosition(), getItemId());
            }
        });
    }

    public View view() {
        return rootLayout;
    }

    public DataType data() {
        return data;
    }

    public void onBind(DataType data) {
        this.data = data;
    }

    public void setOnHolderClickListener(@NonNull final AdapterView.OnItemClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public void clear() {
        data = null;
    }
}