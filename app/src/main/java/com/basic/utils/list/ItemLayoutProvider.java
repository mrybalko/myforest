package com.basic.utils.list;

public interface ItemLayoutProvider {
    int layoutId();
}