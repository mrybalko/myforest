package my.forest.ui.main

import android.support.v7.widget.RecyclerView
import android.view.View
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection
import my.forest.data.TreeData

class TreesGroup(var headerValue: String,
                 var items: MutableList<TreeData>,
                 sectionParameters: SectionParameters?,
                 val itemClickListener: (TreeData) -> Unit
                 ) : StatelessSection(sectionParameters) {

    override fun getContentItemsTotal(): Int {
        return items.size
    }

    override fun onBindItemViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        val itemHolder: GridItemHolder = holder as GridItemHolder

        val data:TreeData = items.get(position)
        val name = data.name
        val iconResId = data.iconResId

        itemHolder.title.text = name
        itemHolder.image.setImageResource(iconResId)
        itemHolder.rootView.setOnClickListener{ itemClickListener.invoke(data) }
    }

    override fun getItemViewHolder(view: View): RecyclerView.ViewHolder {
        return GridItemHolder(view)
    }

    override fun getHeaderViewHolder(view: View): RecyclerView.ViewHolder {
        return HeaderViewHolder(view)
    }

    override fun onBindHeaderViewHolder(holder: RecyclerView.ViewHolder) {
        val headerHolder: HeaderViewHolder = holder as HeaderViewHolder

        headerHolder.titleTv.text = headerValue
    }
}