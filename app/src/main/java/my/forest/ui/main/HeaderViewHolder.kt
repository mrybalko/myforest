package my.forest.ui.main

import android.view.View
import android.widget.TextView
import my.forest.R
import android.support.v7.widget.RecyclerView

class HeaderViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view) {

    private val TAG = "HeaderViewHolder"

    val titleTv: TextView

    init {
        titleTv = view.findViewById<View>(R.id.header_value) as TextView
    }
}
