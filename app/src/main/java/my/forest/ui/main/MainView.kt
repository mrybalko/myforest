package my.forest.ui.main

import com.arellomobile.mvp.MvpView
import my.forest.data.TreeGroupData

interface MainView : MvpView {

    fun updateMyTrees(myTrees: List<TreeGroupData>)

    fun goToAddingMenu()

    fun showAlert(id:String)
}
