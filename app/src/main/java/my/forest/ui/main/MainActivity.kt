package my.forest.ui.main

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager

import kotlinx.android.synthetic.main.activity_main.*
import my.forest.R
import com.arellomobile.mvp.presenter.InjectPresenter
import com.basic.base.BaseActivity
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter
import my.forest.data.TreeGroupData
import android.widget.Button

class MainActivity : BaseActivity(), MainView {

    @InjectPresenter
    lateinit var presenter: MainPresenter

    private lateinit var  sectionAdapter: SectionedRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Fabric.with(this, Crashlytics())

        initRecycler()

        btn_add.setOnClickListener({ presenter.onAddBtnClicked()})
    }

    private fun initRecycler() {

        val layoutManager = GridLayoutManager(applicationContext, 3)
        layoutManager.setSpanSizeLookup(object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                when (sectionAdapter.getSectionItemViewType(position)) {
                    SectionedRecyclerViewAdapter.VIEW_TYPE_HEADER -> return 3
                    else -> return 1
                }
            }
        })

        recycler.setLayoutManager(layoutManager)
        recycler.setHasFixedSize(false)
        recycler.setNestedScrollingEnabled(false)

        sectionAdapter = SectionedRecyclerViewAdapter()
        recycler.setAdapter(sectionAdapter)
    }

    override fun updateMyTrees(myTrees: List<TreeGroupData>) {

        val sectionParameters:SectionParameters = SectionParameters.builder()
                .itemResourceId(R.layout.li_list_trees)
                .headerResourceId(R.layout.li_groups_header)
                .build()

        sectionAdapter.removeAllSections()
        for (group:TreeGroupData in myTrees) {
            if (group.itemsSize() > 0) {
                val section = TreesGroup(
                        group.header,
                        group.items.toMutableList(),
                        sectionParameters,
                        {tree -> presenter.onItemClicked(tree)})

                sectionAdapter.addSection(section)
            }
        }

        sectionAdapter.notifyDataSetChanged()

        updateTreesNumber(myTrees)
    }

    override fun goToAddingMenu() {
        getNavigator().goToAddingMenu(this)
    }

    private fun updateTreesNumber(myTrees: List<TreeGroupData>) {
        var number = 0
        for (group: TreeGroupData in myTrees) {
            number += group.itemsSize()
        }
        trees_number.setText(getString(R.string.trees_number, number.toString()))
    }

    override fun showAlert(id :String) {
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.v_context_menu)
        dialog.getWindow().setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));

        val removeBtn = dialog.findViewById<Button>(R.id.btn_remove)
        removeBtn.setOnClickListener({ view ->
            presenter.onRemoveClicked(id)
            dialog.dismiss()
        })

        dialog.show()
    }
}
