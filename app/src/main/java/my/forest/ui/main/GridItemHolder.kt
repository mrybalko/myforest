package my.forest.ui.main

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import my.forest.R

class GridItemHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val TAG = "TreeItem"

    val title: TextView
    val image: ImageView
    val rootView:View

    init {
        rootView = view!!.findViewById(R.id.rootView)
        title = view!!.findViewById(R.id.title_tree)
        image = view!!.findViewById(R.id.ic_tree)
    }
}