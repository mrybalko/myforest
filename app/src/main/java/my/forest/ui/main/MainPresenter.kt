package my.forest.ui.main

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.basic.base.BasePresenter
import my.forest.R
import my.forest.data.TreeData
import my.forest.data.TreeGroupData
import my.forest.data.TreeType
import my.forest.managers.TreesManager

@InjectViewState
class MainPresenter : BasePresenter<MainView>() {

    private val TAG: String = "MainPresenter"

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        TreesManager.allTreesObserver()
                .subscribe({ it ->
                    run {
                        showAllTrees(it)
                    }
                },
                        { it -> Log.e(TAG, "allTreesObserver", it) })
    }

    override fun attachView(view: MainView?) {
        super.attachView(view)
        updateLocalTrees()

        updateTreesWithAsync()
    }

    private fun updateLocalTrees() {
        TreesManager.getPurchasedTrees()
    }

    private fun updateTreesWithAsync() {
        TreesManager.getPurchasedTreesAsync()
    }

    private fun showAllTrees(allTrees: List<TreeData>) {
        Log.d(TAG, "showAllTrees")

        val treeCategories:MutableList<TreeGroupData> = mutableListOf()

        treeCategories.add(TreeGroupData(getString(R.string.title_premium_tree), extract(TreesManager.premiumTrees(), allTrees)))
        treeCategories.add(TreeGroupData(getString(R.string.title_self_tree), extract(TreesManager.selfGrownTrees(), allTrees)))

        viewState.updateMyTrees(treeCategories)
    }

    fun onAddBtnClicked() {
        viewState.goToAddingMenu()
    }

    private fun extract(templates:List<TreeData>, all:List<TreeData>) : List<TreeData> {
        val list: MutableList<TreeData> = mutableListOf()

        for (item:TreeData in all) {
            for (template:TreeData in templates) {
                if (template.key.equals(item.key)) {
                    list.add(item)
                    continue
                }
            }
        }

        return list
    }

    fun onRemoveClicked(id :String) {
        TreesManager.removeTree(id)
        updateLocalTrees()
    }

    fun onItemClicked(tree: TreeData) {
        if (TreeType.SELF_GROWN.equals(tree.treeType)) {
            viewState.showAlert(tree.id)
        }
    }
}