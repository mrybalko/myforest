package my.forest.ui.choosers.premium

import my.forest.data.TreeData
import my.forest.ui.choosers.ChooserBaseView

interface ChoosePremiumTreesView  : ChooserBaseView {
    fun updatePremiumTrees(availableTrees: List<TreeData>)
    fun updateComingSoonTrees(comingSoonTrees: List<TreeData>)

    fun purchase(sku:String)
}