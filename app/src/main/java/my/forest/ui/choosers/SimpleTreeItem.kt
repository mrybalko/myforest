package my.forest.ui.choosers

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.basic.utils.list.BaseViewHolder
import my.forest.R
import my.forest.data.TreeData

open class SimpleTreeItem(view: View?) : BaseViewHolder<TreeData>(view) {

    private val TAG = "TreeItem"

    private val title: TextView
    private val image: ImageView

    init {
        title = view!!.findViewById(R.id.title_tree)
        image = view!!.findViewById(R.id.ic_tree)
    }

    override fun onBind(data: TreeData) {
        super.onBind(data)
        title.text = data.name
        image.setImageResource(data.iconResId)
    }
}