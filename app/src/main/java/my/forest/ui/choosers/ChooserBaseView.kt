package my.forest.ui.choosers

import com.basic.base.BaseView

interface ChooserBaseView : BaseView {
    fun showSuccessMessage(title:String, message: String)
}