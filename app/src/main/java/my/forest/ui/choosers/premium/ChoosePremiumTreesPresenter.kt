package my.forest.ui.choosers.premium

import android.util.Log
import com.android.billingclient.api.BillingClient
import com.arellomobile.mvp.InjectViewState
import com.basic.base.BasePresenter
import com.basic.base.BillingManager
import my.forest.R
import my.forest.data.Constants
import my.forest.data.TreeData
import my.forest.managers.TreesManager

@InjectViewState
class ChoosePremiumTreesPresenter : BasePresenter<ChoosePremiumTreesView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        fun premiumTreesFetcher(): (list: List<TreeData>) -> Unit = { premiumTrees: List<TreeData> ->
            viewState.updatePremiumTrees(premiumTrees)
        }

        TreesManager.getPremiumTrees(premiumTreesFetcher())
        viewState.updatePremiumTrees(TreesManager.premiumTrees())
        viewState.updateComingSoonTrees(TreesManager.comingSoonPremiumTrees())
        BillingManager.onPurchaseUpdateObservable()
                .subscribe(
                        { sku -> handlePurchaseResponse(Constants.ON_PURCHASED_CODE, sku) },
                        { t -> Log.e("", "onPurchaseUpdateObservable", t) })
    }

    fun onPremiumTreeClicked(tree: TreeData) {
        viewState.purchase(tree.sku)
    }

    fun handlePurchaseResponse(responseCode: Int, sku:String) {

        val treeName: String? = TreesManager.treeNameBySku(sku)

        when (responseCode) {
            BillingClient.BillingResponse.OK -> {}
            BillingClient.BillingResponse.ITEM_ALREADY_OWNED -> viewState.showSuccessMessage("", String.format(getString(R.string.bought_before), treeName))
            Constants.ON_PURCHASED_CODE -> viewState.showSuccessMessage("", String.format(getString(R.string.tree_purchased), treeName))
            else -> viewState.showSuccessMessage("", String.format(getString(R.string.purchase_error), treeName))
        }
    }
}