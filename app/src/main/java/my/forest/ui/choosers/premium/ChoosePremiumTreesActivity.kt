package my.forest.ui.choosers.premium

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.basic.base.BillingManager
import com.basic.utils.list.BaseViewHolder
import com.basic.utils.list.ListAdapter
import com.basic.utils.list.ListDataSet
import kotlinx.android.synthetic.main.ac_choose_premium_trees.*
import my.forest.R
import my.forest.data.TreeData
import my.forest.ui.choosers.ChooserBaseActivity
import my.forest.ui.choosers.SimpleTreeItem

class ChoosePremiumTreesActivity : ChooserBaseActivity(), ChoosePremiumTreesView {

    @InjectPresenter
    lateinit var presenter: ChoosePremiumTreesPresenter

    private lateinit var availableTreesAdapter: com.basic.utils.list.ListAdapter<TreeData>
    private lateinit var comingSoonTreesAdapter: com.basic.utils.list.ListAdapter<TreeData>

    companion object {

        fun newInstance(context: Context): Intent {
            return Intent(context, ChoosePremiumTreesActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.ac_choose_premium_trees)

        initComingSoonRecycler()
        initAvailableRecycler()
    }

    private fun initComingSoonRecycler() {
        recycler_horizontal.setLayoutManager(LinearLayoutManager(baseContext, LinearLayoutManager.HORIZONTAL, false))

        recycler_horizontal.setHasFixedSize(false)
        recycler_horizontal.setNestedScrollingEnabled(false)

        comingSoonTreesAdapter = ListAdapter.createEmptyAdapter(
                { context, view -> SimpleTreeItem(view) },
                { R.layout.li_list_trees })

        comingSoonTreesAdapter.setOnItemClickListener({ adapterView, view, position, id ->
            val data = comingSoonTreesAdapter.getItem(position)
        })
        recycler_horizontal.setAdapter(comingSoonTreesAdapter)
    }

    private fun initAvailableRecycler() {
        recycler_vertical.setLayoutManager(LinearLayoutManager(baseContext, LinearLayoutManager.VERTICAL, false))

        recycler_vertical.setHasFixedSize(false)
        recycler_vertical.setNestedScrollingEnabled(false)

        availableTreesAdapter = ListAdapter.createEmptyAdapter(
                { context, view -> AvailableTreeItem(view) },
                { R.layout.li_add_premium_tree })

        availableTreesAdapter.setOnItemClickListener({ adapterView, view, position, id ->
            val data = availableTreesAdapter.getItem(position)
            presenter.onPremiumTreeClicked(data)
        })
        recycler_vertical.setAdapter(availableTreesAdapter)
    }

    override fun updatePremiumTrees(availableTrees: List<TreeData>) {
        availableTreesAdapter.updateData(ListDataSet(availableTrees))
        availableTreesAdapter.notifyDataSetChanged()
    }

    override fun updateComingSoonTrees(comingSoonTrees: List<TreeData>) {
        comingSoonTreesAdapter.updateData(ListDataSet(comingSoonTrees))
        comingSoonTreesAdapter.notifyDataSetChanged()
    }

    internal inner class AvailableTreeItem(view: View) : BaseViewHolder<TreeData>(view) {

        private val TAG = "AvailableTreeItem"

        private val title: TextView
        private val image: ImageView
        private val price: TextView

        init {
            title = view.findViewById(R.id.title_tree)
            price = view.findViewById(R.id.price)
            image = view.findViewById(R.id.ic_tree)
        }

        override fun onBind(data: TreeData) {
            super.onBind(data)
            title.text = data.name
            image.setImageResource(data.iconResId)
            price.text = data.price
        }
    }

    override fun purchase(sku: String) {
        BillingManager.purchase(sku, this, {responseCode ->
            run { presenter.handlePurchaseResponse(responseCode, sku) }})
    }

}