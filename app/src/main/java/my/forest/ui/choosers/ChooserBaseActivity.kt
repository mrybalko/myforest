package my.forest.ui.choosers

import android.support.constraint.ConstraintLayout
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.basic.base.BaseActivity
import my.forest.R

open class ChooserBaseActivity: BaseActivity(), ChooserBaseView {

    private var messageHolder: ConstraintLayout? = null

    override fun onResume() {
        super.onResume()
        messageHolder = findViewById(R.id.message_holder)
    }


    override fun showSuccessMessage(title: String, message: String) {
        messageHolder?.apply {
            removeAllViews()
            createInfoWindow(title, message, true)
            visibility = View.VISIBLE
        }
    }

    fun createInfoWindow(title:String, message:String, showOkBtn:Boolean): View {
        val infoWindow:View = layoutInflater.inflate(R.layout.v_info_message, messageHolder, true)
        if (!TextUtils.isEmpty(title)) {
            infoWindow.findViewById<TextView>(R.id.title).apply {
                text = title
                visibility = View.VISIBLE
            }
        }

        if (!TextUtils.isEmpty(message)) {
            infoWindow.findViewById<TextView>(R.id.message).apply {
                text = message
                visibility = View.VISIBLE
            }
        }

        if (showOkBtn) {
            infoWindow.findViewById<Button>(R.id.btn).apply {
                visibility = View.VISIBLE
                setOnClickListener { messageHolder?.visibility = View.GONE }
            }
        }

        return infoWindow
    }

}