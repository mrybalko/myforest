package my.forest.ui.choosers.selfgrown

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.StaggeredGridLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.basic.utils.list.ListAdapter
import com.basic.utils.list.ListDataSet
import kotlinx.android.synthetic.main.activity_main.*
import my.forest.R
import my.forest.data.TreeData
import my.forest.ui.choosers.ChooserBaseActivity
import my.forest.ui.choosers.SimpleTreeItem

class ChooseSelfGrownTreesActivity : ChooserBaseActivity(), ChooseSelfGrownTreesView  {

    @InjectPresenter
    lateinit var presenter: ChooseSelfGrownTreesPresenter

    private lateinit var recyclerAdapter: com.basic.utils.list.ListAdapter<TreeData>

    companion object {

        fun newInstance(context: Context): Intent {
            return Intent(context, ChooseSelfGrownTreesActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.ac_choose_self_grown_trees)

        initRecycler()
    }

    private fun initRecycler() {
        recycler.setLayoutManager(StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL))
        recycler.setHasFixedSize(false)
        recycler.setNestedScrollingEnabled(false)

        recyclerAdapter = ListAdapter.createEmptyAdapter(
                { context, view -> SimpleTreeItem(view) },
                { R.layout.li_add_selfgrown })

        recyclerAdapter.setOnItemClickListener({ adapterView, view, position, id ->
            val data = recyclerAdapter.getItem(position)

            presenter.onItemClicked(data)

        })
        recycler.setAdapter(recyclerAdapter)
    }

    override fun updateTrees(myTrees: List<TreeData>) {
        recyclerAdapter.updateData(ListDataSet(myTrees))
        recyclerAdapter.notifyDataSetChanged()
    }
}