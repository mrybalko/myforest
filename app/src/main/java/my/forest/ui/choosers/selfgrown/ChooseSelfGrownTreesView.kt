package my.forest.ui.choosers.selfgrown

import my.forest.data.TreeData
import my.forest.ui.choosers.ChooserBaseView

interface ChooseSelfGrownTreesView : ChooserBaseView {
    fun updateTrees(selfGrownTrees: List<TreeData>)
}