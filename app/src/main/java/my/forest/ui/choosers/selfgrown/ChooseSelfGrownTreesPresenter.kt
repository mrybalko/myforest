package my.forest.ui.choosers.selfgrown

import com.arellomobile.mvp.InjectViewState
import com.basic.base.BasePresenter
import my.forest.R
import my.forest.data.TreeData
import my.forest.data.TreeType
import my.forest.managers.TreesManager

@InjectViewState
class ChooseSelfGrownTreesPresenter : BasePresenter<ChooseSelfGrownTreesView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        viewState.updateTrees(TreesManager.selfGrownTrees())
    }

    fun onItemClicked(data: TreeData) {
        val id:String = data.key + System.currentTimeMillis()
        val newData = data.copy(id = id, treeType = TreeType.SELF_GROWN)
        TreesManager.addTree(newData)
        viewState.showSuccessMessage("", String.format(getString(R.string.tree_added), data.name))
    }
}