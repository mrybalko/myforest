package my.forest.ui.addingmenu

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.TextView
import com.arellomobile.mvp.presenter.InjectPresenter

import com.basic.base.BaseActivity
import com.basic.utils.list.BaseViewHolder
import com.basic.utils.list.ListAdapter
import com.basic.utils.list.ListDataSet
import kotlinx.android.synthetic.main.activity_main.*

import my.forest.R
import my.forest.data.MenuItemData

class AddingMenuActivity : BaseActivity(), AddingMenuView {

    companion object {

        fun newInstance(context: Context): Intent {
            return Intent(context, AddingMenuActivity::class.java)
        }
    }

    @InjectPresenter
    lateinit var presenter: AddingMenuPresenter

    private lateinit var recyclerAdapter: com.basic.utils.list.ListAdapter<MenuItemData>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.ac_adding_menu)

        initRecycler()
    }

    private fun initRecycler() {
        recycler.setLayoutManager(LinearLayoutManager(baseContext, LinearLayoutManager.VERTICAL, false))

        recycler.setHasFixedSize(false)
        recycler.setNestedScrollingEnabled(false)

        recyclerAdapter = ListAdapter.createEmptyAdapter(
                { context, view -> MenuItem(view) },
                { R.layout.li_adding_menu })

        recyclerAdapter.setOnItemClickListener({ adapterView, view, position, id ->
            val data = recyclerAdapter.getItem(position)
            presenter.onItemSelected(data.itemType)
        })
        recycler.setAdapter(recyclerAdapter)
    }

    override fun updateMenuItems(menus: List<MenuItemData>) {
        recyclerAdapter.updateData(ListDataSet(menus))
        recyclerAdapter.notifyDataSetChanged()
    }

    internal inner class MenuItem(view: View) : BaseViewHolder<MenuItemData>(view) {

        private val TAG = "MenuItem"

        private val title: TextView
        private val description: TextView

        init {
            title = view.findViewById(R.id.adding_title)
            description = view.findViewById(R.id.adding_description)
        }

        override fun onBind(data: MenuItemData) {
            super.onBind(data)

            val itemType: MenuItemType = data.itemType
            title.setText(getString(itemType.titleResId).toUpperCase())
            description.setText(getString(itemType.descriptionResId).toUpperCase())
        }
    }

    override fun goToAddingSelfGrownTree() {
        getNavigator().goToSelfGrownTrees(this)
    }

    override fun goToAddingPremiumTree() {
        getNavigator().goToPremiumTrees(this)
    }
}
