package my.forest.ui.addingmenu

import com.arellomobile.mvp.InjectViewState
import com.basic.base.BasePresenter
import my.forest.data.MenuItemData

@InjectViewState
class AddingMenuPresenter : BasePresenter<AddingMenuView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        viewState.updateMenuItems(listOf(
                MenuItemData(MenuItemType.SelfGrownTree),
                MenuItemData(MenuItemType.PremiumTree)))
    }

    fun onItemSelected(itemType: MenuItemType) {

        when(itemType) {

           MenuItemType.SelfGrownTree -> viewState.goToAddingSelfGrownTree()
           MenuItemType.PremiumTree -> viewState.goToAddingPremiumTree()
        }
    }
}