package my.forest.ui.addingmenu

import my.forest.R

enum class MenuItemType(val titleResId: Int, val descriptionResId: Int) {
    SelfGrownTree(R.string.add_self_tree, R.string.add_self_tree_description),
    PremiumTree(R.string.add_premium_tree, R.string.add_premium_tree_description),
}