package my.forest.ui.addingmenu

import com.arellomobile.mvp.MvpView
import my.forest.data.MenuItemData

interface AddingMenuView: MvpView {

    fun updateMenuItems(menus: List<MenuItemData>)

    fun goToAddingSelfGrownTree()
    fun goToAddingPremiumTree()
}