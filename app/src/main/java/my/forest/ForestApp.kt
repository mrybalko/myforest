package my.forest

import android.content.Context
import com.basic.base.utils.SharedPreferencesProvider
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump

class ForestApp : android.support.multidex.MultiDexApplication() {

    init {
        instance = this
    }

    companion object {
        private var instance: ForestApp? = null

        fun applicationContext(): Context {
            return instance!!.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()
        SharedPreferencesProvider.init(baseContext)

        ViewPump.init(ViewPump.builder()
                .addInterceptor(CalligraphyInterceptor(
                        CalligraphyConfig.Builder()
                                .setDefaultFontPath("fonts/Firenight-Regular.otf")
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build())

    }
}