package my.forest.data

import com.google.gson.annotations.SerializedName

data class TreeData(@SerializedName("key") val key:String,
                    @SerializedName("sku") val sku:String,
                    @SerializedName("name") val name:String,
                    @SerializedName("iconResId") val iconResId:Int,
                    @SerializedName("description") val description:String="",
                    @SerializedName("price") val price:String="",
                    val id:String = "",
                    val treeType:String = "")