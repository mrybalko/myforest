package my.forest.data

import my.forest.ui.addingmenu.MenuItemType

data class MenuItemData(val itemType: MenuItemType)