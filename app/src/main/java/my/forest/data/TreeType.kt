package my.forest.data

class TreeType {

    companion object {
        val SELF_GROWN:String = "self_grown"
        val PREMIUM:String = "premium"
    }
}