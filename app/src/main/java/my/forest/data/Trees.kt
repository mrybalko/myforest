package my.forest.data

import my.forest.BuildConfig
import my.forest.ForestApp
import my.forest.R

class Trees {

    companion object {
        val OAK_BELARUS_KEY = "OAK_BELARUS_KEY"
        val OAK_HOLLAND_KEY = "OAK_HOLLAND_KEY"
        val OAK_KEY = "OAK_KEY"
        val BIRCH_KEY = "BIRCH_KEY"
        val CHESTNUT_KEY = "CHESTNUT_KEY"
        val FIR_KEY = "FIR_KEY"
        val APPLE_KEY = "APPLE_KEY"
        val PEAR_KEY = "PEAR_KEY"

        val appId = BuildConfig.APPLICATION_ID

        val OAK_BELARUS = TreeData(OAK_BELARUS_KEY, appId + ".premium.oak.belarus", getString(R.string.oak_tree_from_belarus), R.drawable.oak_belarus)
        val OAK_HOLLAND = TreeData(OAK_HOLLAND_KEY, appId + ".premium.oak.holland", getString(R.string.oak_tree_from_holland), R.drawable.oak_holland)

        val OAK = TreeData(OAK_KEY, "", getString(R.string.oak), R.drawable.oak)
        val BIRCH = TreeData(BIRCH_KEY, "", getString(R.string.birch), R.drawable.chestnut)
        val CHESTNUT = TreeData(CHESTNUT_KEY, "", getString(R.string.chestnut), R.drawable.chestnut)
        val FIR = TreeData(FIR_KEY, "", getString(R.string.fir), R.drawable.fir)
        val APPLE = TreeData(APPLE_KEY, "", getString(R.string.apple), R.drawable.pear)
        val PEAR = TreeData(PEAR_KEY, "", getString(R.string.pear), R.drawable.pear)

        fun getString(resId: Int):String {
            return ForestApp.applicationContext().getString(resId)
        }
    }
}