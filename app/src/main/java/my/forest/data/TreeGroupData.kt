package my.forest.data

data class TreeGroupData(val header:String, val items:List<TreeData>) {

    fun itemsSize(): Int {
        return items.size
    }
}