package my.forest.managers

import android.text.TextUtils
import com.android.billingclient.api.*
import com.basic.base.BillingManager
import com.basic.base.utils.SharedPreferencesProvider
import com.basic.utils.LogUtils
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import my.forest.BuildConfig
import my.forest.data.TreeData
import my.forest.data.Trees

object TreesManager {

    private val TAG: String = "TreesManager"

    private val preferences: SharedPreferencesProvider = SharedPreferencesProvider.instance()
    private val treesAll: BehaviorSubject<List<TreeData>> = BehaviorSubject.create()

    private val selfGrownTrees: List<TreeData> = listOf(
                    Trees.OAK,
                    Trees.BIRCH,
                    Trees.CHESTNUT,
                    Trees.FIR,
                    Trees.APPLE,
                    Trees.PEAR)

    private val premiumTrees: List<TreeData> = listOf(
            Trees.OAK_BELARUS,
            Trees.OAK_HOLLAND)

    private val comingSoonPremiumTrees: List<TreeData> = listOf(
            Trees.CHESTNUT,
            Trees.FIR,
            Trees.APPLE)


    fun addTree(tree: TreeData) {
        preferences.addTree(tree)
    }

    fun removeTree(id:String) {
        val trees = ownTrees().toMutableList()
        val tree:TreeData? = trees.find{ id.equals(it.id) }
        if (tree != null) {
            preferences.removeTree(tree)
        }
    }

    fun ownTrees(): List<TreeData> {
        return preferences.trees
    }

    fun selfGrownTrees():List<TreeData> {
        return selfGrownTrees
    }

    fun premiumTrees():List<TreeData> {
        return premiumTrees
    }

    fun comingSoonPremiumTrees():List<TreeData> {
        return comingSoonPremiumTrees
    }

    private fun getSkus(trees: List<TreeData>):List<String> {
        return trees
                .map {it -> it.sku}
                .filter { it -> !TextUtils.isEmpty(it) }
    }

    fun getPremiumTrees(premiumTreesFetcher: (list: List<TreeData>) -> Unit) {

        getPurchasableTreeWithPrice(premiumTrees, premiumTreesFetcher)
    }

    fun getPurchasableTreeWithPrice(purchasableTrees:List<TreeData>, purchasableTreesFetcher: (list: List<TreeData>) -> Unit) {

        val skuList: List<String> = getSkus(purchasableTrees)
        LogUtils.log(TAG, "getPurchasableTreeWithPrice : skus : " + skuList.toString())

        BillingManager.getProductsDetails(skuList, object : SkuDetailsResponseListener{
            override fun onSkuDetailsResponse(responseCode: Int, skuDetailsList: MutableList<SkuDetails>?) {

                handleSkuDetailsList(responseCode, skuDetailsList, purchasableTrees, purchasableTreesFetcher)
            }
        })
    }

    private fun handleSkuDetailsList(responseCode: Int,
                                     skuDetailsList: MutableList<SkuDetails>?,
                                     purchasableTrees:List<TreeData>,
                                     purchasableTreesFetcher: (list: List<TreeData>) -> Unit) {
        val purchasableTreesWithPrice: MutableList<TreeData> = mutableListOf()
        if (responseCode == BillingClient.BillingResponse.OK) {


            if (skuDetailsList != null) {
                LogUtils.log(TAG, "skuDetailsList size: " + skuDetailsList.size.toString())

                purchasableTreesWithPrice.addAll(mergedPurchasableTreesWithSkuDetails(purchasableTrees, skuDetailsList))

            } else {
                LogUtils.log(TAG, "skuDetailsList null")
                purchasableTreesWithPrice.addAll(purchasableTrees)
            }

        } else if (BuildConfig.DEBUG) {
            LogUtils.log(TAG, "getProductsDetails : response code : " + responseCode.toString())
            purchasableTreesWithPrice.addAll(purchasableTrees)
        }

        purchasableTreesFetcher.invoke(purchasableTreesWithPrice)
    }

    private fun mergedPurchasableTreesWithSkuDetails(purchasableTrees:List<TreeData>, skuDetailsList:List<SkuDetails>): List<TreeData> {
        val treesWithPrice: MutableList<TreeData> = mutableListOf()

        purchasableTrees.forEach { purchasableTree ->
            run {
                var purchasableTreeWithPrice = purchasableTree.copy()

                skuDetailsList.forEach { skuDetailItem ->

                    if (purchasableTree.sku.equals(skuDetailItem.sku)) {
                        purchasableTreeWithPrice = purchasableTree.copy(price = skuDetailItem.price)
                    }
                }
                treesWithPrice.add(purchasableTreeWithPrice)
            }
        }

        return treesWithPrice
    }

    fun getPurchasedTrees() {

        BillingManager.getPurchase{ purchaseResult ->

            val responseCode = purchaseResult.responseCode
            val purchasesList = purchaseResult.purchasesList

            handlePurchaseResult(responseCode, purchasesList)
        }
    }

    fun getPurchasedTreesAsync() {

        BillingManager.getPurchasesAsync(PurchaseHistoryResponseListener { responseCode, purchasesList ->

            if (responseCode == BillingClient.BillingResponse.OK) {
                handlePurchaseResult(responseCode, purchasesList)
            } else {
                LogUtils.log(TAG, "getPurchasesAsync : responseCode : " + responseCode.toString())
            }
        })
    }

    fun handlePurchaseResult(responseCode: Int, purchasesList:List<Purchase>) {
        val purchasableTrees: MutableList<TreeData> = mutableListOf()
        purchasableTrees.addAll(premiumTrees)
        val trees: MutableList<TreeData> = mutableListOf()

        if (responseCode == BillingClient.BillingResponse.OK) {

            LogUtils.log(TAG, "handlePurchaseResult : BillingResponse.OK")

            purchasesList.forEach { purchase ->
                run {
                    purchasableTrees.forEach { purchasableTree ->
                        if (purchase.sku.equals(purchasableTree.sku)) {
                            trees.add(purchasableTree)
                        }
                    }
                }
            }
        } else if (BuildConfig.DEBUG) {
            LogUtils.log(TAG, "getPurchases : response code : " + responseCode.toString())
        }

        LogUtils.log(TAG, "trees : size : " + trees.size.toString())

        treesAll.onNext(collectAllTrees(trees))
    }

    fun collectAllTrees(purchasedTrees:List<TreeData>): List<TreeData> {
        val allTrees: MutableList<TreeData> = mutableListOf()
        allTrees.addAll(purchasedTrees)
        allTrees.addAll(TreesManager.ownTrees())

        return allTrees
    }

    fun allTreesObserver(): Observable<List<TreeData>> {
        return treesAll;
    }

    fun treeNameBySku(sku: String): String? {
        val trees = mutableListOf<TreeData>()
        trees.addAll(selfGrownTrees)
        trees.addAll(premiumTrees)

        val tree: TreeData? = trees.find { it.sku.equals(sku) }
        return tree?.name
    }
}